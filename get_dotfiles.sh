# get vundle:
#git clone https://github.com/VundleVim/Vundle.Vim ~/.vim/bundle/Vundle.vim
# write over existing, bad
#cp ./_vimrc_basic ~/.vimrc
# run basic vimrc setup
#vim +PluginInstall +qall

# get jbarg/dotfiles_new
git clone https://github.com/jbarg/dotfiles_new ~/.dotfiles
cp jbarg_install_link.sh ~/.dotfiles/install/link.sh
cd ~/.dotfiles
./install.sh
# run again with new script
vim +PluginInstall +qall
